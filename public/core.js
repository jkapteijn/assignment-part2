/**
 * Created by jkapteijn on 9/14/14.
 */

(function(angular) {
    function MainController($scope, $http) {

        var searchKey;

        var onCallComplete = function(response){
            $scope.error = '';
            $scope.photos = response.data;

        };

        var onError = function(reason){
            if(reason.data.statusCode == 404){
                    $scope.photos = [];
                    $scope.error = reason.data.message;
            } else {
                $scope.error = 'FlickrSeach webservice is not available, please try again later.';
            }
        };

        $scope.doSearch = function(searchKey){
            $http.get('http://localhost:3000/search?query='+searchKey)
                .then(onCallComplete, onError);
        };
    }

    angular.module("searchApp", []).controller("MainController", ["$scope", "$http", MainController]);

})(angular);



/*
var searchApp = angular.module('searchApp', []);


function MainController($scope, $http){

    $http.get('http://localhost:3000/search/?query=to')
        .success(function(data){
            $scope.photos = data;
            console.log($data);
        })
        .error(function(data){
        console.log('Error: ' + data);
        });

    $scope.doSearch = function(searchKey){
        console.log(searchKey);
        console.log($scope.formData);
        $http.post('http://localhost:3000/search/?',$scope.formData)
            .success(function(data){
                $scope.formData = {};
                $scope.photos = data;
                console.log(data);
            });
    }
}
*/